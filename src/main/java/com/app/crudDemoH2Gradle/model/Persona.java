package com.app.crudDemoH2Gradle.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Model User")
public class Persona implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "el id de la persona", required = true)
	private int id;
	
	@ApiModelProperty(value = "el nombre la persona", required = true)
	private String nombre;
	
	@ApiModelProperty(value = "el dni la persona", required = true)
	private int dni;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	@Override
	public String toString() {
		return "Persona [id=" + id + ", nombre=" + nombre + ", dni=" + dni + "]";
	}

}
