package com.app.crudDemoH2Gradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudDemoH2GradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudDemoH2GradleApplication.class, args);
	}

}
