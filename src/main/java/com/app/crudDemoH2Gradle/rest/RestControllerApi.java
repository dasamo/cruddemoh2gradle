package com.app.crudDemoH2Gradle.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.crudDemoH2Gradle.model.Persona;
import com.app.crudDemoH2Gradle.repository.CrudRepository;

import io.swagger.annotations.Api;

@RestController
@Api(value = "infos", produces = "application/json")
public class RestControllerApi{
	@Autowired
	CrudRepository repo;
	
	
	@GetMapping("lista")
	@ResponseBody
	public List<Persona> lista() {
		
		return repo.lista();
	}
	
	@PostMapping("savePerosona")
	@ResponseBody
	public int save(@RequestBody Persona p) {
		return repo.save(p);
	}
	
	@DeleteMapping("deletePersona/{id}")
	@ResponseBody
	public int delete(@PathVariable int id) {
		
		System.out.println(id);
		return  repo.delete(id);
	}
	
	@PutMapping("updatePersona")
	@ResponseBody
	public int update(@RequestBody Persona p) {
		return repo.update(p);
	}

}
